import yfinance as yf  
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator


# def plot_stocks(tickers, period):
# 	s = ''
# 	[s.append(ticker) for ticker in tickers]
# 	s = s[:-1]
# 	data = yf.download(s, period=period)

tickers = 'XUU.TO XEF.TO XEC.TO HXT.TO ZLB.TO ZAG.TO'
# tickers = 'XUU.TO XEF.TO'

# Get the data for the stock Apple by specifying the stock ticker, start date, and end date
data = yf.download(tickers, period='3y')
 

# add column 'Relative' for % growth since beginning of period
data = data.join(
	data[['Adj Close']].div(data[['Adj Close']].iloc[0]).sub(1).multiply(100).rename(columns={'Adj Close':'Relative'}))

# print(data)
fig, ax = plt.subplots(figsize=(12.,6.75))

ax.plot(data['Relative'])
ax.yaxis.set_minor_locator(AutoMinorLocator(2))
plt.grid(axis='y', which='both')
plt.xticks(rotation=90)
plt.show()